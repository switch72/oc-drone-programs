local component = require("component")
local event = require("event")
local computer= require("computer")
local math = require("math")
local modem = component.modem
local serialization = require("serialization")

port =2222
starting_pos = {0,0,0}
destination = {0,0,0}
math.randomseed(212182)

function split(source, delimiters)
    local elements = {}
    local pattern = '([^'..delimiters..']+)'
    string.gsub(source, pattern, function(value) elements[#elements + 1] =     value;  end);
    return elements
end

function getdistance()
    modem.broadcast(port,"return d.getOffset()")
    _, _, from, port, _, message = event.pull("modem_message")
    response_table=serialization.unserialize(message)
    return tonumber(response_table[1])
end

function adjustCourse()
    os.sleep(.5)
   local start_dist = math.ceil(getdistance())
   local adjustment = {0,0,0} --chooses random axis to adjust
   adjustment[ math.random(1,3)] = 2 --adjust movement by 2 in the chosen axis
    modem.broadcast(port,"d.move("..adjustment[1]..","..adjustment[2]..","..adjustment[3]..")")
    _, _, from, port, _, message = event.pull("modem_message")
    print("Start dist ".. start_dist)
    print("Adjusting Course"..adjustment[1]..","..adjustment[2]..","..adjustment[3])
    os.sleep(1)
    local new_dist = math.ceil(getdistance())
    print("New Dist "..new_dist)
    if new_dist >= start_dist then --if not closer to target, recurse
        adjustCourse()       
    end
      modem.broadcast(port,"d.move("..0-adjustment[1]..","..0-adjustment[2]..","..0-adjustment[3]..")")
      _, _, from, port, _, message = event.pull("modem_message")
end

function monitor_move(prev_dist)
    
        os.sleep(.5)
        local dist_remaining = math.ceil(getdistance())
        print(dist_remaining.." blocks to go.")
        
        if dist_remaining >= prev_dist then --if not making progress adjust course
          adjustCourse()
        end

        dist_remaining = math.ceil(getdistance())
        if dist_remaining > 1 then --if not at destination recurse
           monitor_move(dist_remaining)
        end

end

function move(destination)
    modem.broadcast(port,"d.move("..destination[1]-starting_pos[1]..","..destination[3]-starting_pos[3]..","..destination[2]-starting_pos[2]..")")
    _, _, from, port, _, message = event.pull("modem_message")
    print("Moving...")
    
    monitor_move(getdistance())

    for key,value in ipairs(destination) do
        starting_pos[key] = tonumber(value)
    end
end

modem.open(port)
modem.broadcast(port,"d= component.proxy(component.list('drone')())")
modem.broadcast(port,"rbt= component.proxy(component.list(‘robot’)())")

print("Enter Starting X,Z,Y")
temp_table = split(io.read(),",")

for key,value in ipairs(temp_table) do
    starting_pos[key] = tonumber(value)
end

print(""..starting_pos[1].." "..starting_pos[2].." "..starting_pos[3])

while true do
    print("Enter Destination X,Z,Y")
    temp_table = split(io.read(),",")
    for key,value in ipairs(temp_table) do
        destination[key] = tonumber(value)
    end
    move(destination)
    
end