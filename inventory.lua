
local component = require("component")
local event = require("event")
local computer= require("computer")
local math = require("math")
local modem = component.modem
local serialization = require("serialization")

local port =2222

local charger_coords = {209,49,30}
local inventory1_pull_coords = {196,77,30}
local inventory2_put_coords = {207,79,31}
local inventory2_pull_coords = {206,79,30}
local inventory1_put_coords = {195,77,31}
local waypoint = {208,63,32}
local waypoint2 = {202,75,32}
local waypoint3 = {197,74,38}
local waypoint4 = {197,74,60}
local waypoint5 = {206,74,61}
local inventory3 = {198,71,66}
local starting_pos = {}
for key,value in ipairs(charger_coords) do
    starting_pos[key] = value
end

math.randomseed(212182)
modem.open(port)
modem.broadcast(port,"d=component.proxy(component.list('drone')())")
os.sleep(.1)
modem.broadcast(port,"inv=component.proxy(component.list('inventory_controller')())")
os.sleep(.1)

function dronecommand(command)
  modem.broadcast(port,command)
  _, _, from, port, _, message = event.pull("modem_message")
  response_table=serialization.unserialize(message)
  os.sleep(.1)
  return response_table[1]
end

function split(source, delimiters)
    local elements = {}
    local pattern = '([^'..delimiters..']+)'
    string.gsub(source, pattern, function(value) elements[#elements + 1] =     value;  end);
    return elements
end

function getdistance()
    modem.broadcast(port,"return d.getOffset()")
    _, _, from, port, _, message = event.pull("modem_message")
    response_table=serialization.unserialize(message)
    return tonumber(response_table[1])
end

function adjustCourse()
    os.sleep(.5)
   local start_dist = math.ceil(getdistance())
   local adjustment = {0,0,0} --chooses random axis to adjust
   adjustment[ math.random(1,3)] = 2 --adjust movement by 2 in the chosen axis
    modem.broadcast(port,"d.move("..adjustment[1]..","..adjustment[2]..","..adjustment[3]..")")
    _, _, from, port, _, message = event.pull("modem_message")
    print("Start dist ".. start_dist)
    print("Adjusting Course"..adjustment[1]..","..adjustment[2]..","..adjustment[3])
    os.sleep(1)
    local new_dist = math.ceil(getdistance())
    print("New Dist "..new_dist)
    if new_dist >= start_dist then --if not closer to target, recurse
        adjustCourse()       
    end
      modem.broadcast(port,"d.move("..0-adjustment[1]..","..0-adjustment[2]..","..0-adjustment[3]..")")
      _, _, from, port, _, message = event.pull("modem_message")
end

function monitor_move(prev_dist)
    
        os.sleep(1)
        local dist_remaining = math.ceil(getdistance())
        --print(dist_remaining.." blocks to go.")
        
        if dist_remaining >= prev_dist then --if not making progress adjust course
          adjustCourse()
        end

        dist_remaining = math.ceil(getdistance())
        if dist_remaining > 1 then --if not at destination recurse
           monitor_move(dist_remaining)
        end

end

function move(destination)
    modem.broadcast(port,"d.move("..destination[1]-starting_pos[1]..","..destination[3]-starting_pos[3]..","..destination[2]-starting_pos[2]..")")
    _, _, from, port, _, message = event.pull("modem_message")
    print("Moving")
    
    monitor_move(getdistance())
    os.sleep(.2)
    for key,value in ipairs(destination) do
        starting_pos[key] = value
    end
end


function findinventory()
  command = 'response={} for side=0,5,1 do response[side]=inv.getInventorySize(side) end return response'
  modem.broadcast(port,command)
  _, _, from, port, _, message = event.pull("modem_message")
  local response_table=serialization.unserialize(message)
  return response_table[1]
end


function emptyinventory(side,size)
  for slot=1,size,1 do
    command = "return inv.suckFromSlot("..side..","..slot..")"
    print("Attempting to pull from side "..side.." and slot "..slot)
    modem.broadcast(port,command)
    _, _, from, port, _, message = event.pull("modem_message")
    print(message)
    os.sleep(.2)
  end        
os.sleep(1)    
end

function dumpintoinventory(side,size)
  local startslot = 1
  for droneslot = 1, 4, 1 do
    dronecommand("return d.select("..droneslot..")")
    if dronecommand("return d.count()") > 0 then
      for slot=startslot,size,1 do        
        if dronecommand("return inv.dropIntoSlot("..side..","..slot..")") then
          startslot = slot + 1
          break
        end
      end 
    end   
  end    
  os.sleep(1)  
end

function findfullcards(side,size)
  local fullcards = {}
  for slot=1,size,1 do
    command = "return inv.getStackInSlot("..side..","..slot..")"
    modem.broadcast(port,command)
    _, _, from, port, _, message = event.pull("modem_message")
    local response_table = serialization.unserialize(message)
    if response_table[1] then
      local card_table = response_table[1]
     -- print(tostring(card_table["usedBytes"] / card_table["totalBytes"]))
      if card_table["usedBytes"]/card_table["totalBytes"] > .5 or card_table["storedItemTypes"] > 60 then
        table.insert(fullcards,{side,slot})
      end
    end
    os.sleep(.2)
  end        
  os.sleep(1)  
  return fullcards
end

function pullslots(slotlist)
  startslot = 1
  for key,value in pairs (slotlist) do
    print("pulling card"..tostring(value[2]))
    for slot = startslot, 4, 1 do
      dronecommand("return d.select("..slot..")")
      --print("trying to store in slot "..tostring(slot))
      if dronecommand("return d.count()") == 0 then
        dronecommand("return inv.suckFromSlot("..value[1]..","..value[2]..")") --slurp slurp slurp slurp slurp
        startslot = slot + 1
        break
      end
      if startslot == 5 then 
        print("All slots full")
        return
      end
    end
  end
end


while true do
move(waypoint)
move(inventory2_pull_coords)
emptyinventory(5,6)

move(inventory1_put_coords)
dumpintoinventory(0,6)

---- go upstairs
move(waypoint3)
move(waypoint4)
move(waypoint5)
move(inventory3)
--remove up to 4 mostly full cards from drive
local fullcardtable = findfullcards(4,10)
if #fullcardtable > 0 then
pullslots(fullcardtable)
else
print("No cards need moving.")
end
--return to central computer
move(waypoint5)
move(waypoint4)
move(waypoint3)
if #fullcardtable > 0 then
move(inventory2_put_coords)

dumpintoinventory(0,6) --drop all cards into IOport
move(waypoint2)
move(inventory2_pull_coords)
os.sleep(300) --wait for cards to be emptied into network
emptyinventory(5,6)--pull all the cards out of IOport
--return upstairs
move(waypoint2)
move(waypoint3)
move(waypoint4)
move(waypoint5)
move(inventory3)
dumpintoinventory(4,10) --return all cards to drive


move(waypoint5)
move(waypoint4)
move(waypoint3)
end
move(inventory1_pull_coords)
emptyinventory(4,6)


move(inventory2_put_coords)
dumpintoinventory(0,6)

move(waypoint)

move(charger_coords)
os.sleep(600)
end